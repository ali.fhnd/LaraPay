let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
  'resources/assets/css/cssFonts.css',
  'resources/assets/css/lib/bootstrap/bootstrap-rtl.min.css',
  'resources/assets/css/animate.css',
  'resources/assets/css/spinner.css',
  'resources/assets/css/helper.css',
  'resources/assets/css/fonts.css',
  'resources/assets/css/style.css',
  'resources/assets/css/custom.css',
], 'public/css/admin.min.css');

mix.scripts([
  'resources/assets/js/lib/bootstrap/js/popper.min.js',
  'resources/assets/js/lib/bootstrap/js/bootstrap.min.js',
  'resources/assets/js/jquery.slimscroll.js',
  'resources/assets/js/sidebarmenu.js',
  'resources/assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js',
  'resources/assets/js/custom.min.js',
], 'public/js/admin.min.js');
