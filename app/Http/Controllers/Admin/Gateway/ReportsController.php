<?php

namespace App\Http\Controllers\Admin\Gateway;

use App\Repositories\Contracts\GatewayAggregationRepositoryInterface;
use App\Repositories\Eloquent\Gateway\EloquentGatewayAggregationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportsController extends Controller
{
    /**
     * @var GatewayAggregationRepositoryInterface
     */
    private $gatewayAggregationRepository;

    public function __construct(GatewayAggregationRepositoryInterface $gatewayAggregationRepository)
    {

        $this->gatewayAggregationRepository = $gatewayAggregationRepository;
    }

    public function index()
    {
        $reports = $this->gatewayAggregationRepository->all(null, ['gateway.user']);
        return view('admin.gateway.report.index', compact('reports'));
    }
}
