<?php

namespace App\Http\Controllers\Admin\User;

use App\Events\User\UserRegistered;
use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Services\Notification\Facade\Notification;
use App\Services\Notification\NotificationService;
use App\Services\Notification\NotificationType;
use App\Services\User\UserCreateService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Event;

class UsersController extends Controller
{
    private $usersRepository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->usersRepository = $repository;

    }

    public function index(Request $request)
    {
        $users = $this->usersRepository->all();
        return view('admin.user.index', compact('users'));
    }

    public function create()
    {

        return view('admin.user.create');
    }

    public function store(Request $request)
    {
        $userCreateService = new UserCreateService(
            [
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password
            ]
        );

        $user_create_result = $userCreateService->perform();
        return redirect()->back()->with('success', $user_create_result);

    }

    public function search(Request $request)
    {
       $results = $this->usersRepository->searchUsers($request->search);
       return response()->json(['items' => $results]);
    }
}
