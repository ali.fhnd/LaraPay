<?php

namespace App\Http\Controllers\Admin\User;

use App\Models\UserAccount;
use App\Repositories\Contracts\GatewayRepositoryInterface;
use App\Repositories\Contracts\UserAccountRepositoryInterface;
use App\Repositories\Eloquent\Users\EloquentUserAccountRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserAccountsController extends Controller
{


    /**
     * @var UserAccountRepositoryInterface
     */
    private $user_account_repository;

    public function __construct(UserAccountRepositoryInterface $user_account_repository)
    {

        $this->user_account_repository = $user_account_repository;
    }

    public function index()
    {
        $userAccounts = $this->user_account_repository->all(null, ['owner']);

        return view('admin.user.account.index', compact('userAccounts'));
    }

    public function create()
    {
        $statuses = UserAccount::getStatuses();
        $userAccountItem = null;
        return view('admin.user.account.create', compact('statuses', 'userAccountItem'));
    }

    public function store(Request $request)
    {
        $newUserAccount = $this->user_account_repository->store([
            'user_account_user_id' => $request->owner,
            'user_account_title' => $request->title,
            'user_account_card_number' => $request->account_card,
            'user_account_sheba_number' => $request->account_sheba,
            'user_account_number' => $request->account_number,
            'user_account_status' => $request->account_status
        ]);
        if ($newUserAccount) {
            return back()->with('success', 'حساب جدید با موفقیت اضافه شد');
        }
    }

    public function delete(Request $request, $id)
    {
        $item = $this->user_account_repository->find($id);
        if ($item) {
            $item->delete();

            return back()->with('success', 'حساب بانکی مورد نظر با موفقیت حذف شد!');
        }

        return back()->withErrors([
            'invalid_user_account' => 'حساب کاربری مورد نظر معتبر نمی باشد!'
        ]);
    }

    public function edit(Request $request, $id)
    {
        $userAccountItem = $this->user_account_repository->find($id);
        if (!$userAccountItem) {
            return back()->withErrors([
                'invalid_user_account' => 'حساب کاربری معتبر نمی باشد!'
            ]);
        }
        $statuses = UserAccount::getStatuses();
        return view('admin.user.account.edit', compact('userAccountItem', 'statuses'));
    }

    public function search(Request $request)
    {
        $gateway_id = $request->gateway;
        $gateway_repository = resolve(GatewayRepositoryInterface::class);
        $gateway_item = $gateway_repository->find($gateway_id);
        $results = $this->user_account_repository->findBy([
            'user_account_user_id' => $gateway_item->gateway_user_id
        ], ['user_account_id as id', 'user_account_title as text'], false);
        //$htmlResult = view('admin.user.account.search',compact('results'))->render();
        return response()->json([
            'items' => $results
        ]);
    }
}
