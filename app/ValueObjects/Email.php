<?php


namespace App\ValueObjects;


class Email
{
    private $email;

    public function __construct($value)
    {
        if ( ! filter_var($value, FILTER_VALIDATE_EMAIL)) {
            throw  new \InvalidArgumentException('invalid email address');
        }
        $this->email = $value;
    }

    public function __toString()
    {
        return (string)$this->email;
    }

    private function setEmail($email)
    {
        $this->email = $email;
    }

}