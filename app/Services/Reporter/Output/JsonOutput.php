<?php


namespace App\Services\Reporter\Output;


class JsonOutput implements ReporterOutput
{

    public function output($data)
    {
        if (is_array($data)) {
            return json_encode($data);
        }
    }
}