<?php


namespace App\Services\Notification\Providers\Sms\Gateways;


interface SmsGateway
{
    public function send(string $to,string $message);
}