<?php

namespace App\Services\Payment\Contracts;

use App\Models\Payment\Payment;

interface OnlineGateway
{

    public function paymentRequest();

    public function verifyPayment(array $verifyParams);

    public function gatewayTitle();

    public function setPayment(Payment $payment);

}