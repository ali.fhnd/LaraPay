<?php


namespace App\Services\Payment\Online;


use App\Helpers\Hash\Hash;
use App\Models\Invoice\Invoice;
use App\Models\Payment\Payment;
use App\Models\Payment\PaymentType;
use App\Repositories\Payment\PaymentRepositoryInterface;
use App\Services\Payment\Contracts\PaymentMethod;

class OnlinePayment extends PaymentMethod
{
    protected $gateway;
    protected $invoice;
    protected $paymentRepository;
    protected $title;

    public function __construct(Invoice $invoice)
    {
        $this->invoice           = $invoice;
        $this->paymentRepository = resolve(PaymentRepositoryInterface::class);
        $this->setDefaultGateway();

    }

    public function pay()
    {
        $newPayment = $this->initPayment();
        if ($newPayment) {
            $this->gateway->setPayment($newPayment);

            return $this->gateway->paymentRequest();
        }
    }

    public function verify(Payment $payment, array $verifyParams)
    {
        $providerClass    = $this->getProviderByName($payment->payment_gateway);
        $providerInstance = new $providerClass;
        $providerInstance->setPayment($payment);

        return $providerInstance->verifyPayment($verifyParams);
    }

    protected function initPayment()
    {
        return $this->paymentRepository->create([
            'payment_code'       => Hash::randomCode(),
            'payment_invoice_id' => $this->invoice->invoice_id,
            'payment_type'       => PaymentType::GATEWAY,
            'payment_gateway'    => $this->gateway->gatewayTitle(),
            'payment_amount'     => $this->invoice->invoice_amount,
            'payment_res_num'    => $this->getPaymentResNum(),
            'payment_status'     => Payment::UNPAID
        ]);
    }

    protected function getPaymentResNum()
    {
        return str_replace('.', '', microtime(true));
    }

    private function getProviderByName(string $provider)
    {
        return 'App\\Services\\Payment\\Online\\Gateways\\' . ucfirst($provider);
    }

    private function setDefaultGateway()
    {
        $defaultGateway      = config('gateways.default');
        $defaultGatewayClass = $this->getProviderByName($defaultGateway);
        $this->gateway       = new $defaultGatewayClass;
    }
}