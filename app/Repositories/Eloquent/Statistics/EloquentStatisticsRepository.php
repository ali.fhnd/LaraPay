<?php


namespace App\Repositories\Eloquent\Statistics;


use App\Repositories\Contracts\StatisticsRepositoryInterface;

class EloquentStatisticsRepository implements StatisticsRepositoryInterface
{

    public function totalGateways()
    {
        return 0;
    }

    public function todayTotalTransactions()
    {
        return 0;
    }

    public function todayTotalWithdrawal()
    {
        return 0;
    }

    public function totalPendingGateways()
    {
        return 12656;
    }
}