<?php


namespace App\Entities\User;


use App\Models\User;

class EloquentUserEntityMapper implements UserEntity
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {

        $this->user = $user;
    }

    public function getName(): string
    {
        return $this->user->name;
    }

    public function getEmail(): string
    {
        // TODO: Implement getEmail() method.
    }

    public function getGateways()
    {
        return $this->user->gateways;
    }
}